import {Component, EventEmitter, Output} from '@angular/core';


@Component({
    selector: 'app-header',
    templateUrl : './header.component.html',
    styleUrls: ['./header.component.css']
})

export class HeaderComponent {

    /* @Output() is use to send datas out of the component */

    /* create an emitter to populate data along the parent element*/
    @Output() featureSelected = new EventEmitter<string>();

    constructor() {

    }

    ngOninit() {

    }

    onSelect(feature : string) {
      this.featureSelected.emit(feature)
    }



}
