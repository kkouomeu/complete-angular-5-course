import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {Recipe} from '../model/recipe.model';

@Component({
  selector: 'app-recipe-list',
  templateUrl: './recipe-list.component.html',
  styleUrls: ['./recipe-list.component.css']
})
export class RecipeListComponent implements OnInit {

  @Output() recipeWasSelected = new EventEmitter<Recipe>();

    /* Enable array of recipe as an model */
    recipes: Recipe[] = [
        new Recipe('Simple recipe', 'This is use for testing recipe',
            'https://upload.wikimedia.org/wikipedia/commons/1/15/Recipe_logo.jpeg'),

        new Recipe('Average recipe', 'This is use for cooking time',
            'https://upload.wikimedia.org/wikipedia/commons/1/15/Recipe_logo.jpeg'),

        new Recipe('Hardest recipe', 'This is  for Timechef recipe',
            'https://upload.wikimedia.org/wikipedia/commons/1/15/Recipe_logo.jpeg')
    ];

    constructor() { }

  ngOnInit() {
  }

  onRecipeSelected(recipe : Recipe){
    this.recipeWasSelected.emit(recipe)
  }

}
