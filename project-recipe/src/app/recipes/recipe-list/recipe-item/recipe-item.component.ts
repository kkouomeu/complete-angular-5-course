import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Recipe} from "../../model/recipe.model";

@Component({
  selector: 'app-recipe-item',
  templateUrl: './recipe-item.component.html',
  styleUrls: ['./recipe-item.component.css']
})
export class RecipeItemComponent implements OnInit {

  /* Add @Input() to get datas from outside */
  @Input() recipe: Recipe;

  /* Add @Output() to push datas outside */
  @Output() recipeSelected = new EventEmitter<void>();

  constructor() { }

  ngOnInit() {
  }

  onSelected(){
    this.recipeSelected.emit();
  }

}
