import {Component, EventEmitter, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-cockpit',
  templateUrl: './cockpit.component.html',
  styleUrls: ['./cockpit.component.css']
})
export class CockpitComponent implements OnInit {


    /*Use Output() because we parsing our element out of the component*/
  @Output('srvCreated') serverCreated = new EventEmitter<{serverName : string; serverContent : string}>();
  @Output('bpCreated') blueprintCreated = new EventEmitter<{serverName : string; serverContent : string}>();

  newServerName : string = "" ;
  newServerContent : string = "";

  constructor() { }

  ngOnInit() {
  }

  onAddServer(nameInput){
      this.serverCreated.emit({
          serverName : nameInput.value,
          serverContent : this.newServerContent
      });
  }


  onAddBluePrint(){
      this.blueprintCreated.emit({
          serverName : this.newServerName,
          serverContent : this.newServerContent
      });
  }

}
