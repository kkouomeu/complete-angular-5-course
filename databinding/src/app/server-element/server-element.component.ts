import {
    AfterViewInit,
    Component, ElementRef, Input, OnChanges, OnInit, SimpleChanges, ViewChild,
    ViewEncapsulation
} from '@angular/core';

@Component({
  selector: 'app-server-element',
  templateUrl: './server-element.component.html',
  styleUrls: ['./server-element.component.css'],
  encapsulation : ViewEncapsulation.Emulated //None , Native
})
export class ServerElementComponent implements OnInit,OnChanges, AfterViewInit {

  /* Add decorator to give possibility to parent element to access property*/
  /* We expose #element to the parent component via @Input decorator */
  @Input('srvElement') element : { type : string, name : string, content : string };

  @ViewChild('heading') header : ElementRef;

  constructor() {
      console.log('constructor called !')
  }

  ngOnInit() {
      console.log('ngOnInit called !');
      console.log('Text content : ' + this.header.nativeElement.textContent)
  }

  ngOnChanges(changes : SimpleChanges){
      console.log('ngOnChange called !');
      console.log(changes);
  }

  ngAfterViewInit(){
      console.log('after view init called !')
      console.log('Text content : ' + this.header.nativeElement.textContent)
  }

}
