import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  serverElements = [{
    type: 'server',
    name : 'TestServer',
    content : 'Just a test!'
  }];


  /*This 2 methods are use to update Parent Element (app) when child element added new server*/
    onServerAdded(serverData : {serverName : string; serverContent : string}){
        this.serverElements.push({
            type : 'server',
            name : serverData.serverName,
            content : serverData.serverContent
        })
    }

    onBlueprintAdded(blueprintData : {serverName : string; serverContent : string}){
        this.serverElements.push({
            type : 'blueprint',
            name : 'blue print test',
            content : 'blue print content'
        });
    }
}
